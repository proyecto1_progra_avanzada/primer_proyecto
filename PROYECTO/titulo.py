#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from director_2 import Director
from productora_2 import Productora

class Pelicula():

    def __init__(self):
        self.__titulo = None
        self.__elenco = None
        # año
        self.__fecha_estreno = None
        self.__genero = None
        self.__duration = None
        self.__description = None
        self.__director = None
        self.__productora = None


    def set_titulo(self, nombre):
        if isinstance(nombre, str):
                self.__titulo = nombre

    def get_titulo(self):
        return self.__titulo

    def set_elenco(self, actores):
        self.__elenco = [actores]

    def get_elenco(self):
        return self.__elenco

    def set_fecha(self, fecha):
        if isinstance(fecha, int):
            self.__fecha_estreno = fecha

    def get_fecha(self):
        return self.__fecha_estreno

    def set_genero(self, genero):
        self.__genero = [genero]

    def get_genero(self):
        return self.__genero

    def set_duration(self, tiempo):
        if isinstance(tiempo, int):
            self.__duration = tiempo

    def get_duration(self):
        return self.__duration

    def set_description(self, palabras):
        self.__description = palabras

    def get_description(self):
        return self.__description

    def set_director(self, nombre_d):
        if isinstance(nombre_d, Director):
            self.__director = nombre_d.get_nombre().upper()

    def get_director(self):
        return self.__director

    def set_productora(self, nombre_p):
        if isinstance(nombre_p, Productora):
            self.__productora = nombre_p.get_nombre().upper()

    def get_productora(self):
        return self.__productora

    def imprime_datos(self):
        print("Titulo: {0}\nDirector: {1}\nProductora: {2}".format(self.__titulo, self.__director, self.__productora))
        print("Elenco: ")
        for actor in self.__elenco:
            print(actor)

        print("Genero: ")
        for genero in self.__genero:
            print(genero)

        print("Fecha de estreno: {0}\nDescrición: {1}".format(self.__fecha_estreno,
                                                              self.__description))
        print("la pelicula dura {0} horas con {1} minutos".format(int(self.__duration/60), self.__duration%60))



