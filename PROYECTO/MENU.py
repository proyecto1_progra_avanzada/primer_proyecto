#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import pandas as pd
from titulo import Pelicula
from director_2 import Director
from productora_2 import Productora
'''

BUSCADOR DE PELICULAS

'''
def leer_archivo(fecha):
    '''
    Esta función se encarga de leer el archivo original
    y definir sobre qué columnas y filas se va a trabajar
    para "data" solo están definidas las columnas y para
    "new_data", columnas y filas.
    '''
    archivo = pd.read_csv("IMDb movies.csv")

    data = archivo.filter(["title", "year", "genre",
                           "duration", "director", "production_company",
                           "actors", "description"])
    new_data = data[data["year"] == fecha]

    return new_data

def datos_de_peliculas(data):
    '''
    Esta función se encarga de completar los atributos de cada objeto.

    En el ciclo, se define qué variable contiene cada columna,
    luego se crea el objeto peli de clase Pelicula y se rellenan los atributos
    con las variables definidas anteriormente. Para director y productora se crean
    objetos con sus respectivas clases, se completan los atributos de cada uno y
    después los que faltaban de peli.
    Al finalizar el ciclo, se añade el objeto peli a la lista del incio de la función
    '''
    lista_peliculas = []
    for index, row in data.iterrows():
        titulo = row["title"].upper()
        fecha = row["year"]
        genero = row["genre"]
        duration = row["duration"]
        elenco = row["actors"]
        description = row["description"]
        director = row["director"]
        productora = row["production_company"]



        peli = Pelicula()

        peli.set_titulo(titulo)
        peli.set_fecha(fecha)
        peli.set_genero(genero)
        peli.set_duration(duration)
        if type(elenco) is not float:
            peli.set_elenco(elenco)
        peli.set_description(description)
        if type(director) is not float:
            obj_director = Director()
            obj_director.set_nombre(director)
            peli.set_director(obj_director)
        if type(productora) is not float:
            obj_productora = Productora()
            obj_productora.set_nombre(productora)
            peli.set_productora(obj_productora)
        lista_peliculas.append(peli)



    return lista_peliculas

def busca_por_titulo(peliculas):
    '''
    Primero, se pregunta que película(titulo) desea buscar.
    Luego, se recorre la lista que devolvió la función anterior comparando
    lo que ingresó el usuario con el atributo "titulo" de cada objeto de clase Pelicula.
    Si lo encuentra, llama al método "imprime_datos" que está diseñado para imprimir
    todos los atributos del objeto que encuentre.

    '''
    title = str(input("Ingrese el titulo que desea buscar: ").upper())
    bandera = False
    for movie in peliculas:
        if movie.get_titulo() == title:
            print("Se encontó una similitud: ")
            movie.imprime_datos()
            bandera = True


    if not bandera:
        print("No se encontró coincidencia")

def busca_por_productora(peliculas):
    '''
    Primero, pregunta el nombre de la productora que quiere buscar.
    Luego, recorre "peliculas"(lista con todas las peliculas de el año definido)
    comparando lo que ingresó el usuario con el atributo "productora" del objeto de clase Película.
    si se encuentra una igualdad, va a añadir solo el título de la película a "lista_peliculas".
    inicialmente "bandera" es falso ya que si no se encuentra igualdad, va a utilizar este valor
    para guiarse en que imprimir.
        False = no hay películas para esta productora.
        True = si las hay e imprime "lista_peliculas".

    '''

    productora = str(input("Ingrese el nombre de la productora que desea buscar: ").upper())

    lista_peliculas = []
    bandera = False
    for movie in peliculas:

        if movie.get_productora() == productora:
            lista_peliculas.append(movie.get_titulo())
            bandera = True

    if not bandera:
        print("Esta productora no se encuentra en el reporte")
    else:
        print("Estas son las películas que ha hecho esta productora")
        for peli in lista_peliculas:
            print(peli)

def busca_por_director(peliculas):
    '''
    funciona igual que "busca_por_productora", la unica diferencia es que
    el atributo a comparar es "productor".

    '''

    director = str(input("Ingrese el nombre del director que desea buscar: ").upper())


    lista_peliculas = []
    bandera = False
    for movie in peliculas:

        if movie.get_director() == director:
            lista_peliculas.append(movie.get_titulo())
            bandera = True

    if not bandera:
        print("Este director no se encuentra en el reporte")
    else:
        print("Estas son las películas que tuvieron a este director")
        for peli in lista_peliculas:
            print(peli)

def menu_usuario(peliculas):
    '''
    Esta función hace que tanto "menu", como "menu_usuario" sean recursivas
    hasta que el usuario opte por apretar cualquier tecla que no se haya
    definido en "mini menú"
    '''
    print("Categorias para buscar:\n")
    print(("1 --> Título de película\n2 --> Productora\n3 --> Director"))
    opcion = int(input())
    if opcion == 1:
        busca_por_titulo(peliculas)
    elif opcion == 2:
        busca_por_productora(peliculas)
    elif opcion == 3:
        busca_por_director(peliculas)
    else:
        print("eso no está dentro de las opciones")



    # mini menú
    print("\nsi quiere seguir buscando en este año aprete --> 1")
    print("Si quiere cambiar de año, aprete --> 2")
    print("si quiere salir, aprete --> 3")
    x = int(input())
    if x == 1:
        menu_usuario(peliculas)
    elif x == 2:
        menu()
    elif x == 3:
        print("Que tenga un buen día")

def menu():
    '''
    Esta función primero pregunta el año en que se quieren buscar películas,
    luego llama a las funciones principales: "leer_archivo" y "datos_de_peliculas"
    para que al momento de mostrar el menu al usuario, estén los datos completados
    de cada onbjeto.

    El menú indica sobre que parametros puede buscar.

    '''
    print("Bienvendio a CineInforma")

    fecha = int(input(("\nDefina el año que quiere buscar en el archivo: ")))
    data = leer_archivo(fecha)
    peliculas = datos_de_peliculas(data)
    menu_usuario(peliculas)


try:
    if __name__ == "__main__":
        menu()

except:
   print("ingresó un termino no numérico")